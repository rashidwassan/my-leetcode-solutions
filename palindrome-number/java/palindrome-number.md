# Palindrome Number

###  Given an integer `x`, return `true` if `x` is palindrome integer.
### An integer is a *palindrome* when it reads the same backward as forward.
- For example, 121 is a palindrome while 123 is not.

> Example
```dart
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
```
> Solution
```java
// by converting integer to a string
class Solution {
    public static boolean isPalindrome(int x) {
        // converting number into a StringBuilder object, as it gives reverse() method.
        StringBuilder number = new StringBuilder(String.valueOf(x));
        // since we will need new instance in mem pool
        StringBuilder numberToBeReversed = new StringBuilder(String.valueOf(x));
        numberToBeReversed.reverse();
        return number.compareTo(numberToBeReversed) == 0;
    }
}
```
